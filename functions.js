module.exports = {

    incrementCounter: function(i) {
      if (i == null) {
        var err = new Error();
        err.message = "input must be not null";
        throw err;
      }
      if (isNaN(i)) {
        var err = new Error();
        err.message = "input must be a number";
        throw err;
      }
      if (i<0) {
        var err = new Error();
        err.message = "input must be greater or equal than 0";
        throw err;
      }
      return (i+1).toString();
    },

    waitingTime: function(type, boxes, boxes_time, ordinary, ordinary_time, counters) {
      if (type == null || boxes == null || boxes_time == null || ordinary == null || ordinary_time == null || counters == null) {
        var err = new Error();
        err.message = "input parameters must be not null";
        throw err;
      }
      var result;
      let value_to_add;
      if (type === "boxes"){
        value_to_add = boxes_time/2;
        result = (boxes.length - 1) * boxes_time + ordinary.length * ordinary_time;
      } else if (type === "ordinary") {
        value_to_add = ordinary_time/2;
        if (boxes.length > 1) { result = (ordinary.length - 1) * ordinary_time + (boxes.length - 1) * boxes_time }
        else { result = (ordinary.length - 1) * ordinary_time };
      }
      result += value_to_add;
      if (counters > 0) { result = result/counters };
      return Math.round(result);
    },

    nextTicketToServe: function(boxes, ordinary) {
      if (boxes == null || ordinary == null) {
        var err = new Error();
        err.message = "input must be not null";
        throw err;
      }
      if (!Array.isArray(boxes) || !Array.isArray(ordinary)) {
        var err = new Error();
        err.message = "input must be an array";
        throw err;
      }
      var result;
      if(ordinary.length >= boxes.length){
        result = { structure: ordinary, type: "ordinary" };
        return result;
      } else {
        result = { structure: boxes, type: "boxes" };
        return result;
      }
    },
};
