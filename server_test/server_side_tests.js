/* This file contains all the tests for the server side calls, executed via HTTP */

var chai = require('chai');
var chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
chai.should();

describe("Server", () => {
    describe("GET /", () => {
        it("should respond correctly to starting call", (done) => {
            chai.request(app)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /index", () => {
        it("should respond correctly to index call", (done) => {
            chai.request(app)
                .get('/index')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /customer", () => {
        it("should respond correctly to customer call", (done) => {
            chai.request(app)
                .get('/customer')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /operator", () => {
        it("should respond correctly to operator call", (done) => {
            chai.request(app)
                .get('/operator')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /display", () => {
        it("should respond correctly to display call", (done) => {
            chai.request(app)
                .get('/display')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /ticket", () => {
        it("should respond correctly to ticket call", (done) => {
            chai.request(app)
                .get('/ticket')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /counter_number", () => {
        it("should respond correctly to counter_number call", (done) => {
            chai.request(app)
                .get('/counter_number')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /ready_operator", () => {
        it("should respond correctly to ready_operator call", (done) => {
            chai.request(app)
                .get('/ready_operator')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /ticket_list", () => {
        it("should respond correctly to ticket_list call", (done) => {
            chai.request(app)
                .get('/ticket_list')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("GET /operator_emptiness", () => {
        it("should respond correctly to operator_emptiness call", (done) => {
            chai.request(app)
                .get('/operator_emptiness')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});