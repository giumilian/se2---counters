// require qualcosa per usare funzioni nel file js

const functions = require('../functions');

QUnit.test( "hello test", function( assert ) { // hello world test
    assert.equal( 1, "1");
  });

// test incrementCounter function
  QUnit.test("incrementCounter0", function( assert ) {
    assert.equal(functions.incrementCounter(0), "1");
  });

  QUnit.test("incrementCounter0.5", function( assert ) {
    assert.ok(functions.incrementCounter(1) > 1); // test increasing values
  });

  QUnit.test("incrementCounter1", function( assert ) { //expected error exception
    assert.throws(
      function() {
        functions.incrementCounter("testinginput"); },
      function(error) {
        return error.message === "input must be a number"; });
  });

  QUnit.test("incrementCounter2", function( assert ) { // no input
    assert.throws(
      function() {
        functions.incrementCounter(); },
      function(error) {
        return error.message === "input must be not null"; });
  });

  QUnit.test("incrementCounter3", function( assert ) { // null input
    assert.throws(
      function() {
        functions.incrementCounter(null); },
      function(error) {
        return error.message === "input must be not null"; });
  });

  QUnit.test("incrementCounter4", function( assert ) { // negative input
    assert.throws(
      function() {
        functions.incrementCounter(-1); },
      function(error) {
        return error.message === "input must be greater or equal than 0"; });
  })


// test waitingTime function
QUnit.test("waitingTime1", function( assert ) { // no input
  assert.throws(
    function() {
      functions.waitingTime(); },
    function(error) {
      return error.message === "input parameters must be not null"; });
});

QUnit.test("waitingTime2", function( assert ) { // null input
  assert.throws(
    function() {
      functions.waitingTime(null, null, null, null, null, null); },
    function(error) {
      return error.message === "input parameters must be not null"; });
});


QUnit.test("waitingTime3", function( assert ) { // only boxes, 1 counter
  assert.equal(functions.waitingTime("boxes", [1, 2], 10, [], 5, 1), 15);
});

QUnit.test("waitingTime4", function( assert ) { // only boxes, multiple counters
  assert.equal(functions.waitingTime("boxes", [1, 2], 10, [], 5, 2), 10);
});

QUnit.test("waitingTime5", function( assert ) { // only ordinary, 1 counter
  assert.equal(functions.waitingTime("ordinary", [], 10, [1, 2, 3], 5, 1), 13);
});

QUnit.test("waitingTime6", function( assert ) { // only ordinary, multiple counters
  assert.equal(functions.waitingTime("ordinary", [], 10, [1, 2, 3], 5, 3), 6);
});


QUnit.test("waitingTime7", function( assert ) { // both tickets, 1 counter, served boxes
  assert.equal(functions.waitingTime("boxes", [2, 5], 10, [1, 2, 3], 5, 1), 30);
});


QUnit.test("waitingTime8", function( assert ) { // both tickets, multiple counters, served boxes
  assert.equal(functions.waitingTime("boxes", [2, 5], 10, [1, 2, 3], 5, 2), 18);
});


QUnit.test("waitingTime9", function( assert ) { // both tickets, 1 counter, served ordinary
  assert.equal(functions.waitingTime("ordinary", [2, 5], 10, [1, 2, 3], 5, 1), 23);
});


QUnit.test("waitingTime10", function( assert ) { // both tickets, multiple counters, served ordinary
  assert.equal(functions.waitingTime("ordinary", [2, 5], 10, [1, 2, 3], 5, 3), 9);
});

// test nextTicketToServe function

QUnit.test("nextTicketToServe1", function (assert) {
    assert.deepEqual( functions.nextTicketToServe([], []), { structure: [], type: "ordinary" } );
});

QUnit.test("nextTicketToServe2", function (assert) {
    assert.deepEqual( functions.nextTicketToServe([4], []), { structure: [4], type: "boxes" } );
});

QUnit.test("nextTicketToServe3", function (assert) {
    assert.deepEqual( functions.nextTicketToServe([], [7]), { structure: [7], type: "ordinary" } );
});

QUnit.test("nextTicketToServe4", function (assert) {
    assert.deepEqual( functions.nextTicketToServe([4], [7]), { structure: [7], type: "ordinary" } );
});

QUnit.test("nextTicketToServe5", function (assert) {
    assert.deepEqual( functions.nextTicketToServe([4], [7, 9]), { structure: [7, 9], type: "ordinary" } );
});

QUnit.test("nextTicketToServe6", function (assert) {
  assert.throws(
    function() {
      functions.nextTicketToServe(null, [7, 9]); },
    function(error) {
      return error.message === "input must be not null"; });
});

QUnit.test("nextTicketToServe7", function (assert) {
  assert.throws(
    function() {
      functions.nextTicketToServe([4], null); },
    function(error) {
      return error.message === "input must be not null"; });
});

QUnit.test("systemTest1", function (assert) {
    let boxes = [1, 2];
    let ordinary = [1, 2, 3];

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "ordinary" } );

    ordinary.shift(); // ordinary = [1, 2]

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "ordinary" } );

    ordinary.shift(); // ordinary = [1]

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: boxes, type: "boxes" } );

    boxes.shift(); // boxes = [1]

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "ordinary" } );

    ordinary.shift(); // ordinary []

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: boxes, type: "boxes" } );

    boxes.shift(); // boxes = []

    assert.equal(ordinary.length, 0);
    assert.equal(boxes.length, 0);
});

QUnit.test("systemTest2", function (assert) {
    let next_ordinary = 2;
    let boxes = [1, 2];
    let ordinary = [1];

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: boxes, type: "boxes" } );

    boxes.shift(); // boxes = [1]

    ordinary.push(next_ordinary++); // ordinary = [1, 2]

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "ordinary" } );

    ordinary.shift(); // ordinary = [1]

    assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "ordinary" } );

    ordinary.shift(); // ordinary = []

    //assert.deepEqual( functions.nextTicketToServe(boxes, ordinary), { structure: ordinary, type: "boxes" } );
    assert.equal(ordinary.length, 0);

    boxes.shift(); // boxes = []
    assert.equal(boxes.length, 0);

});

QUnit.test("systemTest3", function (assert) {
    let counters = 0;
    let counters_list = [];

    counters = functions.incrementCounter(counters); // counters = 1
    counters_list.push(counters);

    counters = functions.incrementCounter(counters); // counters = 2
    counters_list.push(counters);

    counters = functions.incrementCounter(counters); // counters = 3
    counters_list.push(counters);

    assert.equal(counters_list.length, 3);

    for(let i = 1; i < counters_list.length; i++) {
        assert.ok(counters_list[i] === counters_list[i-1] + 1);
    }
});
