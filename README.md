# SE2 Group D - Counters

# Group D

Members:

* Giusy Iaria
* Giulia Milan
* Daniele Palumbo
* Enrico Postolov
* Gabriele Telesca

# Instructions

Execute the following commands to run the application:

```
npm install
npm run dev
```

Then browse to localhost:3000.

Display page can be found at localhost:3000/display.

# Tests

Launch unit tests with this simple command:

```
qunit
```

Launch server side tests with:

```
npm run server_test
```