const express = require('express');
const functions = require('./functions');
const app = express();
const port = 3000;

const boxes_serve_time = 10;
const ordinary_serve_time = 5;

var boxes    = 0;
var ordinary = 0;
var counters = 0;
//var list_changed = 0;

var boxes_list    = [];
var ordinary_list = [];
var counter_list  = [];
var tickets_to_serve = [];

var isSomeoneInQueue = 0;

app.use(express.static(__dirname));

//HTML pages redirections
app.get('/index', (req, res) => res.sendFile(__dirname + "/index.html"));
app.get('/customer', (req, res) => res.sendFile(__dirname + "/customer.html"));
app.get('/operator', (req, res) => res.sendFile(__dirname + "/operator.html"));
app.get('/display', (req, res) => res.sendFile(__dirname + "/display.html"));

// Generate a ticket of a specific type passed by param
app.get('/ticket', (req, res) => {
    var type = req.query.type
    var today = new Date();
    var date  = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
    var time  = today.getHours() + ":" + ('0' + today.getMinutes()).slice(-2);
    var output;
    isSomeoneInQueue = 1;

    if(type === 'boxes') {
      boxes = functions.incrementCounter(parseInt(boxes));
      boxes_list.push(boxes)
      console.log(`Save ticket boxes number ${boxes}`);
      var waiting = functions.waitingTime("boxes", boxes_list, boxes_serve_time, ordinary_list, ordinary_serve_time, counter_list.length);
      output = { date: date, time: time, number: boxes, type: type,
        boxes_list: boxes_list.length-1, ordinary_list: ordinary_list.length, waiting_time: waiting };
    } else if(type === 'ordinary') {
      ordinary = functions.incrementCounter(parseInt(ordinary));
      ordinary_list.push(ordinary);
      console.log(`Save ticket ordinary number ${ordinary}`);
      var waiting = functions.waitingTime("ordinary", boxes_list, boxes_serve_time, ordinary_list, ordinary_serve_time, counter_list.length);
      output = { date: date, time: time, number: ordinary, type: type,
        ordinary_list: ordinary_list.length-1, waiting_time: waiting };
    } else {
      output = {}
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(output));
});

// Return a new counter number
app.get('/counter_number', function(req, res) {
  counters = functions.incrementCounter(parseInt(counters));
  counter_list.push(counters);
  res.send(counters);
});

// The operator which is ready will receive the ticket number to serve
app.get('/ready_operator', function(req, res) {
    var output;
    var number = req.query.number;
    let ticket = functions.nextTicketToServe(boxes_list, ordinary_list);
    let ticket_num = ticket.structure.shift();
    let ticket_type = ticket.type;
    if( ticket_num === undefined ) {
        console.log(`No available customers`)
        res.send("-1");
        isSomeoneInQueue = 0;
        return;
    }
    isSomeoneInQueue = 1;
    let ticket_lett = ticket_type == 'ordinary' ? 'O' : 'B';
    tickets_to_serve.push( { ticket_id: ticket_lett + ticket_num, counter: number }); // assume that no operator is able to serve a customer in less than 10 seconds (polling time) (at Poste Italiane it often takes much much more)
    console.log(`Next customer ${ticket_lett + ticket_num} served by ${number}`);
    output = {number: ticket_num, type: ticket_type}
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(output));
});

app.get('/ticket_list', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(tickets_to_serve));
    tickets_to_serve.splice(0, tickets_to_serve.length); // empty the array
});


app.get('/operator_emptiness', function(req, res) {
  if(isSomeoneInQueue != 0){
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(isSomeoneInQueue));
    if(boxes_list.length == 0 && ordinary_list.length == 0){
      isSomeoneInQueue = 0;
    }
  }
  else {
    //console.log("Queue is empty");
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(isSomeoneInQueue));
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = app;
